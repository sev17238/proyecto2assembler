/* Programa que simula el juego en consola en el cual se debe de adivinar la letra faltante de una palabra desplegada al azar.*/
/* Proyecto 3 de assembler */
/* Integrantes: Alejandro Tejada, Diego Sevilla */
/* Carne: 17584, 17238 */
@----------------------------------
.text
.align 2
.type main,%function
.global main


main:	
	/* grabar registro de enlace en la pila */
	stmfd	sp!, {lr}

    /* Se despliega el mensaje de bienvenida */
	ldr r0,=wellcomeMessage
	bl puts

comienzoJuego:
/*Ciclo donde se genera el random*/

	/*Invocamos el turno del jugador 1*/
	turnoj1:
		bl turnoJugador1@se llama al jugador 1
	turnoj2:
		bl turnoJugador2 @se  llama al jugador 2
	comprobarciclo:@se comprueba la cantidad de vueltas
		ldr r4,=vuelta@ se carga el valor de vuelta
		ldr r1,[r4]
		add r1,r1,#1
		str r1,[r4]
		cmp r1,#5
		blt comienzoJuego@si es menor a 5 da la vuelto
		beq  salidajuego@sino sale del juego

	salidajuego:@para salir del juego
		/*Se imprime los resultados*/
		ldr r0,=enter
		bl puts
		ldr r0,=ResumenJuego1
		bl puts
		ldr r0,=ResumenJuego2
		ldr r4,=punteo1
		ldr r1,[r4]
		bl printf
		ldr r0,=ResumenJuego3
		ldr r5,=punteo2
		ldr r1,[r5]
		bl printf
		/*Se comprueba cual jugador ganó*/
		ldr r4,=punteo1
		ldrb r12,[r4]
		ldr r5,=punteo2
		ldrb r11,[r5]
		cmp r12,r11
		beq Riguales
		bgt Rj1mayor
		blt Rj1menor


		/*Mensajes de salida para ver que jugador ganó*/
Riguales:
		ldr r0,=empateJ
		bl puts

		ldr r0,=enterMessage
		bl puts
		ldr r0,=formatoC
		ldr r1,=entrada
		bl scanf

		ldr r0,=happyFace
		bl puts
		bl fin
Rj1menor:
		ldr r0,=ganoJ2
		bl puts

		ldr r0,=enterMessage
		bl puts
		ldr r0,=formatoC
		ldr r1,=entrada
		bl scanf

		ldr r0,=happyFace
		bl puts
		bl fin
Rj1mayor:
		ldr r0,=ganoJ1
		bl puts

		ldr r0,=enterMessage
		bl puts
		ldr r0,=formatoC
		ldr r1,=entrada
		bl scanf

		ldr r0,=happyFace
		bl puts
		bl fin




fin:	
	/* salida correcta */
	ldr r0,=salida
	bl puts
	mov r0, #0
	mov r3, #0
	ldmfd sp!, {lr}	/* R13 = SP */
	bx lr


/*CODIGO JUGADOR 1--------------------------------------------------------------------------------------------------------*/
turnoJugador1:
	ldr r0,=player1Message
	bl puts

	ldr r0,=palabraMessage @@Aqui se despliega la palabra buscada al azar
	bl puts

	ciclo1:
		bl aleatorios
		and r0,#63
		cmp r0,#1
		blt ciclo1
		cmp r0,#38
		bgt ciclo1
		/*Guardamos en el N el valor del random*/
		ldr r10,=N
		str r0,[r10]
	ldr r7,=arrayPalabras
	ldr r6,=N @este seria el RANDOM de 0 a 39
	ldr r5,[r6]
	ldr r9,=multi @se otiene el multiplicador 14 en r9
	ldr r9,[r9]
	mul r5,r5,r9 @aqui se multiplica el random por 14 para que quede en el comienzo de alguna palabra del vector
	add r7,r7,r5 @se suma el random multiplicado por 14 a r7 que contiene el vector de palabras
	mov r8, #14
	printString1:
		ldr r1,[r7] @ r1 toma el valor de la dirreccoin r7 que seria el vector				
    	ldr r0,=formatoC @es caracter 
    	bl printf
		add r7,r7,#1
		sub r8,r8,#1
		cmp r8,#0
		bne printString1

	ldr r0,=enter
	bl puts

	ldr r0,=letraMessage @@Aqui se pide que se ingrese una letra para ver si se adivina la faltante
	bl puts
	bl getchar
	ldr r1,=entrada
	str r0,[r1]
	bl getchar
	

	/*Prueba de impresion de la utlima letra almacenada en arrayPalabras*/
	ldr r7,=arrayLetras
	ldr r6,=N @este seria el RANDOM de 0 a 39
	ldr r5,[r6],#4
	add r7,r7,r5 @se suma el random para obtener cualquier letra de las 40 almacenadas
	
	/*Se verifica si es el mismo caracter*/
    ldr r4,=entrada
    ldrb r1,[r4]
    ldrb r11,[r7]
    cmp r1,r11
    beq EXITOJ1
    b 	NOEXITOJ1
	continua:
		bl turnoj2
/*Se imprime el punte y se guarda el valor si acertó*/
EXITOJ1:
	ldr r0,=BienMessage
	ldr r4,=punteo1
	ldr r1,[r4]
	add r1,r1,#5
	ldr r4,=punteo1
	str r1,[r4]
	bl printf
	ldr r0,=enter
	bl puts
	bl continua
	
/*Se resta del punteo si falló y se imprime*/
NOEXITOJ1:
	ldr r0,=MalMessage
	ldr r12,=punteo1
	ldr r1,[r12]
	subs r1,r1,#2
	ldr r4,=punteo1
	str r1,[r4]
	bl printf
	ldr r0,=enter
	bl puts
	bl continua

/*CODIGO JUGADOR 2--------------------------------------------------------------------------------------------------------*/
turnoJugador2:
	ldr r0,=player2Message
	bl puts

	ldr r0,=palabraMessage @@Aqui se despliega la palabra buscada al azar
	bl puts
	ciclo2:
		bl aleatorios
		and r0,#63
		cmp r0,#1
		blt ciclo2
		cmp r0,#38
		bgt ciclo2
		/*Guardamos en el N el valor del random*/
		ldr r10,=N
		str r0,[r10]
	ldr r7,=arrayPalabras
	ldr r6,=N @este seria el RANDOM de 0 a 39
	ldr r5,[r6]
	ldr r9,=multi @se otiene el multiplicador 14 en r9
	ldr r9,[r9]
	mul r5,r5,r9 @aqui se multiplica el random por 14 para que quede en el comienzo de alguna palabra del vector
	add r7,r7,r5 @se suma el random multiplicado por 14 a r7 que contiene el vector de palabras
	mov r8, #14
	printString2:
		ldr r1,[r7] @ r1 toma el valor de la dirreccoin r7 que seria el vector				
    	ldr r0,=formatoC @es caracter 
    	bl printf
		add r7,r7,#1
		sub r8,r8,#1
		cmp r8,#0
		bne printString2

	ldr r0,=enter
	bl puts

	ldr r0,=letraMessage @@Aqui se pide que se ingrese una letra para ver si se adivina la faltante
	bl puts
	bl getchar
	ldr r1,=entrada
	str r0,[r1]
	bl getchar
	

	/*Prueba de impresion de la utlima letra almacenada en arrayPalabras*/
	ldr r7,=arrayLetras
	ldr r6,=N @este seria el RANDOM de 0 a 39
	ldr r5,[r6],#4
	add r7,r7,r5 @se suma el random para obtener cualquier letra de las 40 almacenadas
	
	/*Se verifica si acertó*/
    ldr r4,=entrada
    ldrb r1,[r4]
    ldrb r11,[r7]
    cmp r1,r11
    beq EXITOJ2
    b 	NOEXITOJ2
	continua2:
		bl comprobarciclo
/*se imprime y se aumenta el contador*/
EXITOJ2:
	ldr r0,=Bien2Message
	ldr r4,=punteo2
	ldr r1,[r4]
	add r1,r1,#5
	ldr r4,=punteo2
	str r1,[r4]
	bl printf
	ldr r0,=enter
	bl puts
	bl continua2
	
/*Se imprime y se resta el contador*/
NOEXITOJ2:
	ldr r0,=Mal2Message
	ldr r12,=punteo2
	ldr r1,[r12]
	subs r1,r1,#2
	ldr r4,=punteo2
	str r1,[r4]
	bl printf
	ldr r0,=enter
	bl puts
	bl continua2



.data
.align 2


/*Arreglos*/
arrayPalabras:
	.asciz "ig_esia      ","plan_ta      ","mas_pan      ","d_dalo       ","dadi_oso     ","e_imero      ","super_luo    ","inefa_le     ","e_ereo       ","in_esante    ", "peren_e      ","luminis_encia","ade_an       ","exh_usto     ","_poca        ","re_iliencia  ","melan_olia   ","el_cuencia   ","efer_escencia","ser_no       ", "so_ambulo    ","alb_         ","ar_ebol      ","halla_go     ","ep_fania     ","a_rora       ","e_resar      ","e_ilogo      ","an_elo       ","es_ena       ", "_topia       ","es_upor      ","e_ilio       ","ca_ejo       ","le_ltad      ","su_lime      ","sor_ilegio   ","desco_cierto ","ma_uiavelico ","esca_par     "

arrayLetras:
	.byte 'l','e','a','e','v','f','f','b','t','c','n','c','m','a','e','s','c','o','v','e', 'n','a','r','z','i','u','g','p','h','c', 'u','t','x','d','a','b','t','n','q','m'

/* Variables para alamacenar valores*/
N: .word 39 @@largo de los vectores
multi: .word 14 @El numero de espacios para llegar a la siguiente palabra
punteo1: .word 0
punteo2: .word 0
vuelta: .word 0
entrada: .asciz "	"



/* Mensajes en consola */
wellcomeMessage: .asciz "\nHola!!! Bienvenido al juego de 'Adivina Letras'!!\nEn este juego deberan de adivinar las letras faltantes en las palabras mostradas para acumular puntos. Cada vez que se acierte se le sumaran 5 puntos y cada vez que falle se le restaran 2 puntos.\nComencemos!!! :D\n"
enterMessage: .asciz "\nPresiona enter para terminar.\n"

player1Message: .asciz "*****JUGADOR 1*****\n"
BienMessage: .asciz "BIEN! Puntos jugador1: %d\n"
MalMessage: .asciz "ERROR! Puntos jugador1: %d\n"

player2Message: .asciz "*****JUGADOR 2*****\n"
Bien2Message: .asciz "BIEN! Puntos jugador2: %d\n"
Mal2Message: .asciz "ERROR! Puntos jugador2: %d\n"

letraMessage: .asciz "Ingrese la letra faltante: "
palabraMessage: .asciz "La palabra es: "

OptionMessage: .asciz "Que desea hacer?\n1. Volver a jugar\n2. Salir\n "
salida: .asciz "Feliz dia, gracias por jugar!! Vuelva pronto!!\n"
mal: .asciz "Ingreso incorrecto.\n"

ResumenJuego1: .asciz "El resumen de los puntos es: \n"
ResumenJuego2: .asciz "JUGADOR 1: %d\n"
ResumenJuego3: .asciz "JUGADOR 2: %d\n"
letra:
	.asciz " Caracter ingresado: %c\n"

ganoJ1: .asciz "!FELICIDADES JUGADOR 1 GANASTE¡"
ganoJ2: .asciz "!FELICIDADES JUGADOR 2 GANASTE¡"
empateJ: .asciz "!HUBO UN EMPATE, ALGO INAUDITO, JUEGUEN OTRA VEZ¡"

/*Ascii Art*/
happyFace: .asciz "                   `,,,,,,,,,,,,,,,,,,,,,`\n                `,,,,,,,,,,,,,,,,,,,,,,,,,,,` \n            .,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,` \n           .,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,. \n          .,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,. \n         .,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,` \n        `,,,,,,,,,,+@@@@,,,,,,,,,,,,,,,,,,,,,,,,,,` \n        ,,,,,,,,,,,@@@@@,,,,,,,,,,,,,:@@@@,,,,,,,,, \n       .,,,,,,,,,,,@@@@@,,,,,,,,,,,,,#@@@@:,,,,,,,,. \n      `,,,,,,,,,,,,@@@@@,,,,,,,,,,,,,@@@@@:,,,,,,,,, \n      ,,,,,,,,,,,,:@@@@@,,,,,,,,,,,,,@@@@@:,,,,,,,,,.  \n     `,,,,,,,,,,,,,@@@@@,,,,,,,,,,,,,@@@@@:,,,,,,,,,, \n     .,,,,,,,,,,,,,@@@@+,,,,,,,,,,,,,@@@@@,,,,,,,,,,,. \n      ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,@@@@@,,,,,,,,,,, \n     `,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,`  \n      `,,,,,,,,:@',,,,,,,,,,,,,,,,,,,,,,,:@+,,,,,,,, \n       .,,,,,,,,:@#,,,,,,,,,,,,,,,,,,,,,+@',,,,,,,,` \n        ,,,,,,,,,,##+,:,,,,,,,,,,,,:,,:@@,,,,,,,,,, \n        `,,,,,,,,,,:#@+,,,,,,,,,,,,,;@@',:,,,,,,,, \n         .,,,,,,,,,,,:#@@+',,,,,;+@@#;,,,,,,,,,:,` \n          .,,,,,,,,,,,,,:+#@@@@@##;,,,,,,,,,,,,,`  \n           .,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,. \n            `,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,`  \n              `,,,,,,,,,,,,,,,,,,,,,,,,,,,,,`  \n                 .,,,,,,,,,,,,,,,,,,,,,,,.\n                     `,,,,,,,,,,,,,,,,`\n"


/* variables para la impresion correcta, decisiones y lectura de datos */
formatoD: .asciz " %d"
formatoC: .asciz "%c"
formatoS: .asciz "%s"
format: .asciz "%d\n"
opcion: .word 0
enter: .asciz "\n"


